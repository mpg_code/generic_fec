# Generic FEC #

### What is this repository for? ###

A cpp implementation of RTP Payload Format for Flexible Forward Error Correction (FEC) (draft-ietf-payload-flexible-fec-scheme-08) in Live555.
Version 1.0

https://tools.ietf.org/html/draft-ietf-payload-flexible-fec-scheme-08

### How do I get set up? ###

* compile
* run

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Simen Fonnes simenfon@ifi.uio.no